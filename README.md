Attempt at a Responsive Grid boilerplate using only CSS.

Handy for situations where I need a grid without a full packaged framework such as Foundation or Bootstrap, which are useful tools in the right circumstance but I wanted to understand how to implement a grid system (albeit a basic one) and provide an example in my portfolio.

Example can be found here (http://wednesday5thaugust2015.meteor.com)